--- d7 upgrade -- 
This module requires http://drupal.org/project/menu_block
This module handles 'book page' titles, but requires the menu block to create a 
list. Again this module doesn't rewrite the titles of book's default block.    

-- SUMMARY --

Book Chapters module adds automatic and dynamic numbering to book node titles.

For a full description of the module, visit the project page:
  http://drupal.org/project/bookchapters

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/bookchapters


-- REQUIREMENTS --

Book module enabled. For more information, visit the handbook page:
  http://drupal.org/handbook/modules/book


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.



-- CONTACT --

Current maintainers:
* Tomi Mikola (TomiMikola) - http://drupal.org/user/183191


This project has been sponsored by:
* National Land Survey of Finland
    Visit http://www.nls.fi/ for more information.
* Mearra
    Professional Drupal consulting in Europe.
    Visit http://mearra.com/ for more information.
